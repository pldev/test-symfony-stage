# Symfony - Test de compétences ! (v1.1.1)

## Préface

Bienvenue aventurier en herbe ! PL.Dev Corp a besoin de toi pour explorer l'univers !

De nombreuses planètes (ou "projets" en langage ancien) restent encore à découvrir... Ta mission, si tu l'acceptes : développer notre technologie de conquête !

## Épisode 1 : Que la flotte soit avec toi !

### 1. Bundle

A l'évidence, nous n'allons pas conquérir les planètes à pieds.

Nous attendons de toi que tu construises une `FactoryBundle` pour créer des vaisseaux qui nous permettront de voyager dans l'espace.

### 2. Entités

#### 2.1. Spaceship

Nos superbes vaisseaux comporteront plusieurs caractéristiques.

**Spaceship** doit déterminer, au minimum, son `name`, son `type` (allié ou ennemi), ses `hp` (points de vie), ses `hpMax`, ses `damages` 
(dégâts par tir).

#### 2.2. Shield

Étant donné que nous parlions de conquête, ce qui peut impliquer d'éventuelles altercations plus ou moins accidentelles et unilatérales, chaque vaisseau devra être protégé par un bouclier énergétique dernier cri.

**Shield** doit déterminer, au minimum, ses `sp` (points de bouclier).

#### 2.3. Passenger

Le pilote automatique, c'est bien, mais... Rien ne vaut des cobayes... Euh... De vaillants explorateurs humains à bord. Enfin... "Humains"... N'ayez aucun scrupule, ce ne sont que des clones sacrifiables.

**Passenger** doit déterminer, au minimum, son `name`, si `isCaptain` (ou pas).

### 3. Relations

Vaisseaux, boucliers et passagers sont de grands inséparables.

Un `Spaceship` a obligatoirement un `Shield` qui lui est associé (à lui seul).

Un `Passenger` doit obligatoirement se trouver dans un `Spaceship`. S'ils restaient sur Terre, ils pourraient être tentés de prendre la place de leur original ou pire, d'exiger des droits des clones ! Et puis quoi encore ?

Un `Spaceship` peut bien évidemment contenir plusieurs `Passenger`. Mais il ne doit jamais être vide : à quoi servirait-il donc ?

### 4. Formulaires, vues et actions

Maintenant que vous avez tout compris au sujet des vaisseaux, de leurs boucliers et de leurs passagers, il faut mettre l'usine en fonctionnement !

#### 4.1. Paperasse administrative

Le désordre est le plus grand ennemi de la productivité. Commençons par concevoir un registre des vaisseaux.

L'URL `/spaceship` doit afficher la liste des `Spaceship`, triés par `type`, en vert les alliés, en rouge le reste de l'univers (ne prenons pas le risque d'être trahis, considérons-les antagonistes par défaut).

Ce registre, doit afficher, au minimum, les informations suivantes : nom, type, HP, HP max, SP.

Par la suite de cette mission, vous intégrerez au registre les fonctionnalités de création, de modification et de suppression de vaisseaux, détaillées ci-dessous.

#### 4.2. Création

Bon, le registre semble un peu vide pour l'instant... Il faut y remédier ! Nous allons y recenser nos vaisseaux, ainsi que ceux ennemis.

L'URL `/spaceship/new` doit permettre la création d'un `Spaceship` en renseignant obligatoirement `name`, `type`, `hpMax` et `damages` dans un formulaire adéquat.

Un nouveau `Shield` lui est automatiquement associé avec des `sp` entre 20 et 30 aléatoirement.

On y intègre un nombre aléatoire de `Passenger` allant de 5 à 10, parfois capitaine et parfois non, pour l'égalité hasardeuse des chances. Il peut donc y avoir plusieurs capitaines par vaisseau. Ils n'auront qu'à se débrouiller pour l'organisation.

#### 4.3. Modification

L'URL `/spaceship/{id}/edit` doit permettre la modification d'un `Spaceship` nous permet de le réparer en augmentant ses HP (attention au minimum de 1 et aux hpMax) !

On peut aussi l'utiliser pour réduire ses `hp` mais, entre nous, ça n'a pas grand intérêt... Vous ne voudriez tout de même pas saborder la mission, n'est-ce pas ?

#### 4.4. Suppression

D'après les plus récentes études ergonomiques, il faut toujours placer un gros bouton rouge dans chaque vaisseau, à côté de la machine à café : le bouton d'auto-destruction instantanée, bien sûr.

Le `Spaceship` est détruit, et bien évidemment son `Shield` et chaque `Passenger` aussi. Pauvres clones... Mais bon, ils sont facilement remplaçables !

#### 4.5. Ready!? Fight!

Quel beau registre... Tous ces jolis petits vaisseaux si bien listés... Si calmes. Si paisibles.

Quel ennui. Déclarons la guerre !

L'URL `/spaceship/ready/fight` doit envoyer deux `Spaceship` combattre en duel.
 
Accèder à cette URL a pour effet de sélectionner un allié et un ennemi aléatoires. Pas de négociation, pas de trêve, peu importe la force de l'ennemi, on l'attaque puis on le laisse riposter bien gentiment. Et à la fin, il ne peut en rester qu'un !

Il faut d'abord détruire leur `Shield` en faisant tomber ses `sp` à 0 (heureusement le `Shield` ne peut pas être réparé). Une fois celui-ci détruit, les dégâts s'appliquent aux `hp` du `Spaceship`.

Attention ! Si les `hp` d'un `Spaceship` descendent à 0, il s'autodétruit et tout ce qui lui est associé avec lui, ses `Passenger` compris pour éviter qu'ils ne tombent aux mains de l'ennemi. Nous ne voulons pas risquer que les ennemis récupèrent des informations sur PL.Dev Corp.

Il faut afficher (directement dans le controller) un rapport de combat selon ce modèle :

```html
Le vaisseau allié Artémis avec 21 d'attaque, 200 HP et 29 SP se joint au combat !
Le vaisseau ennemi Horizon avec 5 d'attaque, 300 HP et 20 SP se joint au combat !
 
Le vaisseau ennemi subit 21 de dégâts !
Le vaisseau a maintenant 299 HP.
 
Le vaisseau allié subit 5 de dégâts !
Le vaisseau a maintenant 200 HP et 24 SP.
 
...
 
Le vaisseau ennemi subit 21 de dégâts !
Le vaisseau a maintenant 0 HP.
 
Le vaisseau ennemi Horizon est détruit !
```

(Attention aux cas d'erreurs !)


## Épisode 2 : Le stalking est source de pouvoir.

Avoir un registre des vaisseaux, c'est bien. Avoir le détail des `Passenger` dans chaque vaisseau, c'est mieux ! Répandre l'information, c'est encore mieux...

Pour cela, nous souhaitons donner un accès public à l'URL `/api/enemy/stalk` en méthode POST qui aura pour effet d'envoyer nos espions sur les différents vaisseaux ennemis.

Appeler cette API a pour effet de retourner un tableau JSON contenant les informations de nos espions selon ce modèle :

```json
[
  {
    "10": {
      "name": "Horizon",
      "passengers": [
        "Captain PASSENGER-591092f64a254",
        /*...*/
        "PASSENGER-591092f64a3bd"
      ]
    }
  },
  {
    "11": {
      "name": "Death Star",
      "passengers": [
        "PASSENGER-591b32c844224",
        /*...*/
        "Captain PASSENGER-591b32c844299"
      ]
    }
  }
]
```

Death Star !? Mais, ça semble dangereux !

Nous devons pouvoir passer un paramètre à l'appel de l'API pour pouvoir cibler un `Spaceship` par son `name`. L'espion nous fait alors son rapport selon le même modèle mais cette fois en contenant uniquement le `Spaceship` visé.


## Épisode 3 : Le Guide du voyageur galactique

Nous pouvons encore être plus efficaces dans notre conquête de l'espace... Il pourrait s'avérer utile d'obtenir des détails concernant les planètes qui s'y trouvent afin de préparer nos clones à toutes éventualités.

Pour cela, il existe déjà quelques part un "Guide du voyageur galactique" contenant les informations précieuses dont nous avons besoin. Tu devrais regarder du côté de cette API : [SWAPI](http://swapi.co/).

L'URL `/api/planets` en méthode GET doit servir de tunnel et retourner la première page JSON de la liste des planètes du Guide SWAPI.


## GAME OVER

Félicitations ! C'est la fin de cette mission mais peut-être le début d'une aventure en tant qu'explorateur. :)

Mot de l'auteur : J'espère que tu as pris autant de plaisir à réaliser cet exercice que j'en ai pris à le rédiger ! ^_^

Made with love by [Sullivan B](https://fr.linkedin.com/in/sbethoua).
